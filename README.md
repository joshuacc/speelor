# Speelor

To execute the main program, first run `npm install` to fetch dependencies, then run `node index.js resources/{wordlistName}`. 

There are two wordlists available in `./resources`: `shortlist` and `longlist`.

Unit tests can be run with `npm test`.

To generate misspellings run `node misspell.js resources/{wordlistName}`.

To pipe misspellings into into speelor, run `node misspell.js resources/{wordlistName} | node index.js resources/{wordlistName} > output.txt`.

Speelor may take a while to initialize with the longer word list because it does a fair bit of processing to turn the list into a data structure that is very easy to search.

The misspelling generator appears to have a bug which will occasionally insert newlines into the middle of one of its misspelled words. These may initially look like bugs in Speelor, but I've been able to manually verify that removing the newlines results in a correct spelling suggestion.