var _ = require("lodash");
var lev = require("fast-levenshtein");

var removeDupChars = function (memo, char) {
    if (char != memo[memo.length - 1]) {
        memo += char;
    }

    return memo;
};

var genKeyOf = function (word) {
    var key = word.toLowerCase();
    key = key.replace(/e|i|o|u/g, "a");
    key = _.reduce(key, removeDupChars, "");
    return key;
};

exports.genKeyOf = genKeyOf;

var arrToMap = function (memo, word) {
    var key = genKeyOf(word);

    if (typeof memo[key] != "object") {
        memo[key] = Object.create(null);
    }

    memo[key][word] = true;

    return memo;
};

var createDictionaryMap = function (dictionary) {
    return _.reduce(dictionary, arrToMap, Object.create(null));
};

exports.createDictionaryMap = createDictionaryMap;

var sortByEditDistance = function (wordList, baseline) {
    return wordList.sort(function(a, b) {
        var aDist = lev.get(a, baseline);
        var bDist = lev.get(b, baseline);
        return aDist - bDist;
    });
}

exports.sortByEditDistance = sortByEditDistance;

var getSuggestionsFor = function (map, word) {
    var key = genKeyOf(word);
    var suggestionMap = map[key];

    if (suggestionMap == null) {
        return "NO SUGGESTION";
    }

    if (suggestionMap[word]) {
        return word;
    }

    var suggestions = _.keys(suggestionMap);

    if (_.size(suggestions) === 1) {
        return _.first(suggestions);
    }

    var suggestionsByDistance = sortByEditDistance(suggestions, word);

    return _.first(suggestionsByDistance);
};

exports.getSuggestionsFor = getSuggestionsFor;

var Checker = function Checker (dictionary) {
    if (!(dictionary instanceof Array)) {
        throw new Error("Array required");
    }

    var map = createDictionaryMap(dictionary);

    this.getSuggestionsFor = _.bind(getSuggestionsFor, null, map);
};

exports.Checker = Checker;
