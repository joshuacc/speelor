var speelor = require("./lib/speelor.js");
var fs = require("fs");
var _ = require("lodash");

var wordlist = process.argv[2];

var dictionary = fs.readFileSync(wordlist).toString().split("\n");

var spellchecker = new speelor.Checker(dictionary);

process.stdin.resume()
process.stdin.setEncoding('utf8');
process.stdout.write("> ");
process.stdin.on('data', function(chunk) {
    var input = chunk.split("\n");
    _.forEach(input, function(word) {
        if (word !== "") {
            var suggestion = spellchecker.getSuggestionsFor(word);
            process.stdout.write(suggestion + "\n");
            // Useful for debugging
            //if (suggestion == "NO SUGGESTION") {
            //    console.log("WORD:" + word);
            //}
            process.stdout.write("> ");
        }
    });
});
