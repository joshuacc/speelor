var fs = require("fs");
var _ = require("lodash");

function duplicateLetters (word) {
    var randomIndex = Math.floor(Math.random() * word.length);
    var randomRepeat = 1 + Math.floor(Math.random() * 5);
    var randomChar = word[randomIndex];

    var insert = "";

    for (var i = 0; i < randomRepeat; i++) {
        insert += randomChar;
    }

    var newWord = word.substring(0, randomIndex + 1) +
                  insert +
                  word.substring(randomIndex + 1, word.length);

    return newWord;
}

function misCapitalize (word) {
    var randomIndex = Math.floor(Math.random() * word.length);
    var newWord;
    
    if (word.length != 0) {
        newWord = word.substring(0, randomIndex) +
                  word[randomIndex].toUpperCase() +
                  word.substring(randomIndex + 1, word.length);
    } else {
        newWord = word;
    }

    return newWord;
}

var vowels = ["a","e","i","o","u"];

function wrongVowel (word) {
    var randomIndex = Math.floor(Math.random() * 5);
    var randomVowel = vowels[randomIndex];
    return word.replace(/a|e|i|o|u/i, randomVowel);
}

function allWrong (word) {
    return wrongVowel(duplicateLetters(misCapitalize(word)));
}

var dispatch = [
    duplicateLetters,
    misCapitalize,
    wrongVowel,
    allWrong
];

var wordlist = process.argv[2];

var dictionary = fs.readFileSync(wordlist).toString().split("\n");

_.forEach(dictionary, function(word, index) {
    var misspellFn = dispatch[index % 4];
    process.stdout.write(misspellFn(word) + "\n");
});
