var expect = require("chai").expect;
var sp = require("../lib/speelor.js");

describe("speelor", function() {
    describe("genKeyOf", function() {
        it("should create key with all chars represented in lower case", function() {
            expect(sp.genKeyOf("BCD")).to.equal("bcd");
            expect(sp.genKeyOf("bcd")).to.equal("bcd");
            expect(sp.genKeyOf("BcD")).to.equal("bcd")
        });

        it("should create key with repeat chars represented once", function() {
            expect(sp.genKeyOf("bbb")).to.equal("b");
            expect(sp.genKeyOf("bbbcddd")).to.equal("bcd");
        });

        it("should create key with all vowels represented by 'a'", function() {
            expect(sp.genKeyOf("a")).to.equal("a");
            expect(sp.genKeyOf("e")).to.equal("a");
            expect(sp.genKeyOf("i")).to.equal("a");
            expect(sp.genKeyOf("o")).to.equal("a");
            expect(sp.genKeyOf("u")).to.equal("a");
        });

        it("should apply transformations in the correct order", function() {
            expect(sp.genKeyOf("aeiou")).to.equal("a");
            expect(sp.genKeyOf("AaEee")).to.equal("a");
        });
    });

    describe("createDictionaryMap", function() {
        it("should return a map object which is not an instance of Object", function () {
            var map = sp.createDictionaryMap([]);
            expect(map).to.be.an("object");
            expect(map instanceof Object).to.equal(false);
        });

        it("should return a map with submaps which are not instances of Object", function() {
            var map = sp.createDictionaryMap(["x"]);
            expect(map.x).to.be.an("object");
            expect(map.x instanceof Object).to.equal(false);
        });

        it("should turn an array of words into a map of maps of words", function() {            expect(sp.createDictionaryMap(["Sparta", "Athens"])).to.deep.equal({
                sparta: { Sparta: true },
                athans: { Athens: true }
            });
        });

        it("should group words with the same key together", function() {
            expect(sp.createDictionaryMap(["hit", "hot"])).to.deep.equal({
                hat: {
                    hit: true,
                    hot: true
                }
            });
        });
    });

    describe("sortByEditDistance", function () {
        var words = ["foot", "feet"];
        it("should sort words by their distance from a baseline word", function () {
            expect(sp.sortByEditDistance(words, "fiet")).to.deep.equal(["feet", "foot"]);
            expect(sp.sortByEditDistance(words, "fiot")).to.deep.equal(["foot", "feet"]);
        });
    });

    describe("getSuggestionsFor", function () {
        var map = sp.createDictionaryMap([
            "sheep",
            "people",
            "inside",
            "job",
            "wake",
            "conspiracy"
        ]);

        var multipleCorrectionsMap = sp.createDictionaryMap(["feet", "foot"]);

        it("should suggest corrections for repeated characters", function () {
            expect(sp.getSuggestionsFor(map, "sheeeeep")).to.equal("sheep");
            expect(sp.getSuggestionsFor(map, "peepple")).to.equal("people");
            expect(sp.getSuggestionsFor(map, "jjoobbb")).to.equal("job");
        });

        it("should suggest corrections for miscapitalizations", function () {
            expect(sp.getSuggestionsFor(map, "inSIDE")).to.equal("inside");
        });

        it("should suggest corrections for wrong vowels", function () {
            expect(sp.getSuggestionsFor(map, "weke")).to.equal("wake");
        });

        it("should suggest corrections for combinations of previous 3 mistakes", function () {
            expect(sp.getSuggestionsFor(map, "CUNsperrICY")).to.equal("conspiracy");
        });

        // Is this the intended design? There are no specs for handling
        // a word that is already correct.
        it("should suggest input word if it is already a correct word (even when other possibilities are available)", function() {
            expect(sp.getSuggestionsFor(multipleCorrectionsMap, "foot")).to.equal("foot");
            expect(sp.getSuggestionsFor(multipleCorrectionsMap, "feet")).to.equal("feet");
        });

        it("should suggest 'nearest' correction if multiple corrections are available", function () {
            expect(sp.getSuggestionsFor(multipleCorrectionsMap, "fiot")).to.equal("foot");
            expect(sp.getSuggestionsFor(multipleCorrectionsMap, "fiet")).to.equal("feet");
        });

        it("should return 'NO SUGGESTION' if there is no match", function () {
            expect(sp.getSuggestionsFor(map, "sheeple")).to.equal("NO SUGGESTION");
        });
    });

    describe("Checker", function() {
        it("should construct spellchecker from an array of words", function() {
            expect(new sp.Checker(["words"])).to.be.an.instanceof(sp.Checker);
        });

        it("should throw an error if no array is given", function() {
            var thrower = function () { new sp.Checker() }
            expect(thrower).to.throw(Error);
        });

        it("should provide spelling suggestions on checker instances", function() {
            var myChecker = new sp.Checker(["foot", "feet"]);
            expect(myChecker.getSuggestionsFor("fiet")).to.equal("feet");
        });
    });
});

